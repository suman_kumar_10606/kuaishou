
--------------------------------
create table dim_ticket_group (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
ticket_group nvarchar(255)
)

insert into dim_ticket_group (ticket_group)
select distinct ticket_group from tpind_stg_email where ticket_group not in (select ticket_group from dim_ticket_group)

---------------------------------------

create table dim_ticket_status (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
ticket_status nvarchar(255)
)

insert into dim_ticket_status (ticket_status)
select distinct ticket_status_ from tpind_stg_email where ticket_status_ not in (select ticket_status from dim_ticket_status)


---------------------

create table dim_agent_bpo (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
agent_bpo nvarchar(255)
)


insert into dim_agent_bpo (agent_bpo)
select distinct agent_bpo from tpind_stg_email_qc where agent_bpo not in (select agent_bpo from dim_agent_bpo)


----------------------

create table dim_agent_language (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
agent_language nvarchar(255)
)


insert into dim_agent_language(agent_language)
select distinct agent_language from tpind_stg_email_qc where agent_language 
not in (select agent_language from dim_agent_language)


------------------------------
create table dim_case_channel (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
case_channel nvarchar(255)
)


insert into dim_case_channel(case_channel)
select distinct case_channel from tpind_stg_email_qc where case_channel 
not in (select case_channel from dim_case_channel)

------------------------------------------

create table dim_month (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
[month] nvarchar(255)
)

insert into dim_month([month])
values 
('January'),('February'),('March'),('April'),('May'),('June'),('July'),('August'),('September'),
('october'),('November'),('December')

---------------------------

create table dim_year (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
[year] int
)

insert into dim_year([year])
values 
(2018),(2019),(2020),(2021),(2022),(2023),(2024),(2025)
go 

--------------------

create table dim_client_type (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
client_type nvarchar(255)
)

insert into dim_client_type(client_type)
select distinct client_type from tpind_stg_email_csat where client_type 
not in (select client_type from dim_client_type)


--------------------------
create table dim_locations
(
id int primary Key Identity(1,1),
location_name nvarchar(255),
createdon datetime default getdate(),
isactive bit default 1
) ;

insert dim_locations (location_name) values ('Egypt'),('Indonesia'),('India'),('Pakistan'),('Colambia') ;

-----------------

create table dim_agents
(
id int primary Key Identity(1,1),
agent_name nvarchar(255),
agent_email nvarchar(255),
ccmsid int,
agent_language nvarchar(255),
agent_location nvarchar(255),
createdon datetime default getdate(),
isactive bit default 1
) ;

insert into dim_agents (agent_name,agent_email , ccmsid ,agent_language ,agent_location ) 
select distinct agent,null as email,null as ccmsid,null as agent_language,'India' as location 
from tpind_stg_email_qc where agent not in (select  agent_name from dim_agents)

----------------------- tpegy_chat_voc_raw

insert into dim_agents (agent_name,agent_email , ccmsid ,agent_language ,agent_location ) 
select distinct null as agent,agent_email as email,null as ccmsid,null as agent_language,'egypt' as location 
from tpegy_chat_voc_raw where agent_email not in (select  agent_email from dim_agents)

----------------------- tpegy_chat_volume_raw

insert into dim_agents (agent_name,agent_email , ccmsid ,agent_language ,agent_location ) 
select distinct null as agent,agent_email as email,null as ccmsid,null as agent_language,'egypt' as location 
from tpegy_chat_volume_raw where agent_email not in (select  agent_email from dim_agents)

---------------------------------------------

create table dim_chat_department (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
chat_department nvarchar(255)
)

insert into dim_chat_department(chat_department)
select distinct chat_department from tpegy_chat_volume_raw where chat_department 
not in (select chat_department from dim_chat_department)

-----------------------------

create table dim_chat_completion_status (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
chat_completion nvarchar(255)
)

insert into dim_chat_completion_status(chat_completion)
select distinct chat_completion from tpegy_chat_volume_raw where chat_completion 
not in (select chat_completion from dim_chat_completion_status)

----------------------- tpegy_chat_email_abc_raw

create table dim_lob (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
lob nvarchar(255),
location nvarchar(255)
)

insert into dim_lob(lob,location)
select distinct lob,'egypt' from tpegy_chat_email_abc_raw where lob 
not in (select lob from dim_lob)

-------------------

create table dim_chat_email_abc_status (
id int primary key identity(1,1),
inserted_on datetime default getdate(),
status nvarchar(255)
)

insert into dim_chat_email_abc_status(status)
select distinct status from tpegy_chat_email_abc_raw where status 
not in (select status from dim_chat_email_abc_status)


-----------------

insert into dim_agents (agent_name,agent_email , ccmsid ,agent_language ,agent_location ) 
select distinct agent_name as agent,null as email,ccms_id as ccmsid,null as agent_language,'egypt' as location 
from tpegy_chat_email_abc_raw 
where ccms_id not in (select  coalesce(ccmsid,'') from dim_agents)

-------------------








